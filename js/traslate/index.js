//Ejecutamos la funcion que realiza el cambio de path, para el cambio de pagina sea de inglés a español o viceversa
var check = document.querySelector(".check");
check.addEventListener('click',idioma);

function idioma(){
    let id=check.checked;
    function getUrl(){
        //Se obtiene el valor de la URL desde el navegador
        var actual = window.location+'';
        //Se realiza la división de la URL
        var split = actual.split("/");
        //Se obtiene el ultimo valor de la URL
        var id = split[split.length-1];
        return id;
   }
   var ruta = getUrl();
    if(id==true){
        location.href="eng/"+ruta;
    }else{
        location.href="../"+ruta;
    }
    
}